let app = new Vue({
	el: '#exchange',
	// Basically global variables being accessible from the this. syntax. 
	data: {
		data: null,
		exg: "SEK",
		selectedFromCurrency: "SEK",
		selectedToCurrency: "USD",
		inputAmount: 1,
		outputAmount: 0,

		allCurrencies: "",
		baseRate: "",
	},
	watch: {
		// As soon as the input value in the text field changes the
		// value is being recalculated.   
		inputAmount: function(userInput) {
			if (userInput != "") {
				this.inputAmount = userInput;
				this.outputAmount = this.baseRate[this.selectedToCurrency] * this.inputAmount;
			}
		},		

	},
	created() {
		// The currency shown by default (SEK), and related rates.
		axios.get('https://api.exchangeratesapi.io/latest?base=SEK')
			.then(response => {
				const currencyInfo = response.data;
				const ratesArray = currencyInfo.rates;

				this.exg = currencyInfo.base;
				this.allCurrencies = ratesArray;
				this.baseRate = ratesArray;

				this.selectedFromCurrency = currencyInfo.base;
				this.inputAmount = ratesArray.SEK;
				this.outputAmount = ratesArray.USD;				
			})	
	},
	mounted() {

	},
	methods: {
		// The method receives a mouse click response
		changeCurrency: function(event) {
			const clickedHref = event.target.text;
			axios.get('https://api.exchangeratesapi.io/latest?base=' + clickedHref)
				.then(response => {
					const currencyInfo = response.data;
					const ratesArray = currencyInfo.rates;

					this.exg = currencyInfo.base;
					this.allCurrencies = ratesArray;
				})
		},

		calculateValueFromNewBase: function() {
			axios.get('https://api.exchangeratesapi.io/latest?base=' + this.selectedFromCurrency)
			.then(response => {
				const currencyInfo = response.data;
				const ratesArray = currencyInfo.rates;

				this.baseRate = ratesArray;
				this.outputAmount = this.baseRate[this.selectedToCurrency] * this.inputAmount;
			})
		}, 

		calculateValue: function() {
			// Notice that the regular syntax this.allCurrencies(this.selectedToCurrency)
			// or the same expression with declared variables wont work even thoug this 
			// would be the normall way to acces alements in the array. Only this bracket[]
			// specific syntax, used below, will suffice. 
			this.outputAmount = this.baseRate[this.selectedToCurrency] * this.inputAmount;
		},

		swapCurrency: async function() {
			let temp = this.selectedFromCurrency;
			this.selectedFromCurrency = this.selectedToCurrency;
			this.selectedToCurrency = temp;
	
			this.calculateValueFromNewBase();

			// Saved alternative for Eriks own personal needs:
			// This alternative syntax specifically requires the `` in combination with ${} around the object.
			// const response = await axios.get(`https://api.exchangeratesapi.io/latest?base=${this.selectedFromCurrency}`); 
		}
	}
})
